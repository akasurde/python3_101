#!/usr/bin/env python3
import argparse
import os
import sys


def file_read(file_name):
    """Return file content.
    """
    content = None
    with open(file_name, 'r') as f:
        content = f.readlines()
    return content


def process_operator(operand_one, operand_two, operator):
    """ Fill this
    """
    operand_one = int(operand_one)
    operand_two = int(operand_two)

    if operator == "+":
        return operand_one + operand_two
    elif operator == "-":
        return operand_one - operand_two
    elif operator == "*":
        return operand_one * operand_two
    elif operator == "/":
        return operand_one / operand_two

def parse_options():
    parser = argparse.ArgumentParser(prog='MySciCalc3',
                                     epilog="Copyright: (C) 2018",
                                     description='My First Python 3 Calculator')
    parser.add_argument('--input_file', help='user input file for this calculator')
    parser.add_argument('--answer_file', help='output answer file for this calculator')
    args = parser.parse_args()
    file_name = args.input_file
    output_file = args.answer_file

    if not file_name:
        parser.print_help()
        sys.exit(-1)
 
    if os.path.exists(file_name):
        if not os.path.isfile(file_name):
            sys.exit("Please provide input file")
    else:
        with open(file_name, "w") as f:
            f.write("")
        sys.exit("Done")
        
    # Add exception handling here
    user_input = file_read(file_name)
    for line in user_input:
        line = line.strip()
        operand_one, operator, operand_two = line.split(" ", 3)
        answer = process_operator(operand_one, operand_two, operator)
        with open(output_file, "a+") as writ: 
            writ.write("%s %s %s = %s\n" % (operand_one, operator, operand_two, answer))


# This is one line comment
def main():
    """
    This is skeleton function to learn more about python
    """
    parse_options()

if __name__ == "__main__":
    main()
